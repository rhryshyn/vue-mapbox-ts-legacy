import { getCurrentInstance, Ref, ref, UnwrapRef, watch } from 'vue';
import equal from 'fast-deep-equal';
import { isPrimitive } from './refForClasses';


export default function <T, U=Ref<T> | UnwrapRef<T>>(propertyValue:Record<string,any>, eventName:string|string[] = 'change', modelName='value'):U{
  const instance = getCurrentInstance();
  const eventNames = eventName instanceof Array
    ? eventName
    : [eventName];

  if(!instance)
    throw new Error('vmodel: Instance not found');

  const internal = ref(instance.proxy.$props[modelName] || propertyValue);

  for(const eventName of eventNames)
    watch(internal, _val => {
      instance.proxy.$emit(eventName, internal.value);
    });

  watch(instance.proxy.$props, () => {
    if(isPrimitive(instance.proxy.$props[modelName])
      ? instance.proxy.$props[modelName] !== internal.value
      : !equal(instance.proxy.$props[modelName], internal.value)
    )
      internal.value = instance.proxy.$props[modelName] as UnwrapRef<T>;

  });

  return internal as any;
}
