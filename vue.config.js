module.exports = {
  publicPath: '/',
  'transpileDependencies': [
    'vuetify'
  ],
  configureWebpack: config => {
    enableWebPackSourceMaps(config);
    // enableExternals(config);
  },
  chainWebpack: config => {
    config.externals({
      'vue': 'Vue'
    });
  }

};

function enableExternals(config){
  config.externals = {
    'vue': 'Vue',
  };
}

function enableWebPackSourceMaps(config){
  if (process.env.NODE_ENV === 'development') {
    config.devtool = 'eval-source-map';
    config.output.devtoolModuleFilenameTemplate = info =>

      info.resourcePath.match(/\.vue$/) && !info.identifier.match(/type=script/)
        ? `webpack-generated:///${info.resourcePath.split('/').filter( i => i !== '.').join('/')}?${info.hash}`
        : `webpack-original:///${info.resourcePath.split('/').filter( i => i !== '.').join('/')}`;

    config.output.devtoolFallbackModuleFilenameTemplate = 'webpack:///[resource-path]?[hash]';
  }
}