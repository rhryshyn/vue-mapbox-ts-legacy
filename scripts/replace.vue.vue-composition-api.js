var replace = require('replace');

replace({
  regex: '\'@vue/composition-api\'',
  replacement: '\'vue\'',
  paths: ['./src'],
  recursive: true,
  silent: false
});

replace({
  regex: 'import { ComponentInternalInstance }',
  replacement: 'import { }',
  paths: ['./src'],
  recursive: true,
  silent: false
});

replace({
  regex: 'as any as Component',
  replacement: 'as any',
  paths: ['./src/components'],
  recursive: true,
  silent: false
});

replace({
  regex: 'import { Component,',
  replacement: 'import {',
  paths: ['./src/components'],
  recursive: true,
  silent: false
});


// replace({
//   regex: '{ length: 10, type: \'alphanumeric\' }',
//   replacement: '{ length: 10, type: \'alphanumeric\' } as any',
//   paths: ['./src/components'],
//   recursive: true,
//   silent: false
// });


replace({
  regex: 'ComponentInternalInstance',
  replacement: 'any',
  paths: ['./src'],
  recursive: true,
  silent: false
});

replace({
  regex: 'import \'../@types/vue.composition.api.compatibility\';',
  replacement: 'import \'./@types/vue.composition.api.compatibility\';',
  paths: ['./src/index.ts'],
  recursive: false,
  silent: false
});
